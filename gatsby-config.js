console.log("process.env.NODE_ENV", process.env.NODE_ENV);
module.exports = {
	pathPrefix: process.env.PATH_PREFIX || "/",
	siteMetadata: {
		title: `Example`,
		description: `Example error repo`,
		siteUrl: `https://example.org/`
	},
	plugins: [
		{
			resolve: `gatsby-plugin-netlify-cms`,
			options: {
				enableIdentityWidget: false,
				modulePath: `${__dirname}/src/cms/cms.js`,
				manualInit: true
			}
		}
	]
};
