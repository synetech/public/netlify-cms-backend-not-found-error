import CMS from "netlify-cms-app";
import multipleUpload from "./widgets/multipleUpload/multipleUpload";

CMS.init({
	config: {
		backend: {
			name: "gitlab",
			branch: process.env.GATSBY_CMS_BRANCH || "master",
			repo: "synetech/public/netlify-cms-backend-not-found-error",
			auth_type: "implicit",
			app_id: process.env.GATSBY_APP_ID || ""
		},
		media_folder: "static/uploads",
		public_folder: "/../../../../uploads",
		collections: [
			{
				name: "uploadImages",
				label: "Upload multiple images",
				files: [
					{
						name: "uploadImagesFile",
						label: "Upload multiple images",
						file: "src/content/images/images.json",
						extension: "json",
						fields: [
							{
								label: "Images to upload",
								name: "images",
								widget: "multipleUpload"
							}
						]
					}
				]
			}
		]
	}
});

CMS.registerWidget("multipleUpload", multipleUpload);

// CMS.registerPreviewTemplate("images", errorCatchingImagesPreview);
