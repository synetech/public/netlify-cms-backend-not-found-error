import React from "react";
import { connect } from "react-redux";
import {
	persistMedia,
	loadMedia
} from "netlify-cms-core/dist/esm/actions/mediaLibrary";
// import MediaLibrary from "netlify-cms-core/dist/esm/components/MediaLibrary/MediaLibrary";

const map = {
	persistMedia,
	loadMedia
};

class MultipleUpload extends React.Component {
	render() {
		const onFileUpload = e => {
			const { persistMedia } = this.props;

			e.preventDefault();
			if (e.target.files.length) {
				const { id } = e.target;
				const arrFiles = Array.from(e.target.files);
				const files = arrFiles.forEach(async file => {
					await persistMedia(file);
				});
			}
		};
		return (
			<input
				id={1}
				accept=".jpeg, .pdf"
				type="file"
				onChange={onFileUpload}
				required
				multiple
			/>
		);
	}
}

MultipleUpload.propTypes = {
	// entry: PropTypes.shape({
	// 	getIn: PropTypes.func.isRequired
	// }).isRequired
};

export default connect(
	state => {
		const { mediaLibrary } = state;
		return {
			config: mediaLibrary.get("config")
		};
	},
	map,
	(stateProps, dispatchProps, ownProps) => {
		return { ...ownProps, ...stateProps, ...dispatchProps };
	},
	{ withRef: true }
)(MultipleUpload);
